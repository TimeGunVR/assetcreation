@echo off
REM Copy Assets
for /r %%d in (*.gltf, *.bin, *.png, *.dds) do (
  xcopy /y "%%d" "%~dp0\..\assets\"
  xcopy /y "%%d" "%~dp0\..\assets\"
  echo.
)